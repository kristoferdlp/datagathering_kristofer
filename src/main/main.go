package main

import (
  "os"
  "db"
  "fmt"
  "lib"
  "seed"
)

func main() {

  args := os.Args[1:]
  if (len(args) < 1) {
    fmt.Println("\nUsage: go run src/main/main.go [options]\n")
    fmt.Println("Possible Options:")
    fmt.Println("seed - Seed states and company sizes")
    return
  }

  db.Connect()
  lib.Init()

  if (args[0] == "seed") {
    fmt.Println("Seeding...")
    seed.Seed()
  } else if (args[0] == "reset") {
    fmt.Println("Resetting...")
    lib.Reset()
  }



  // lib.State_all()
  //db.Disconnect()

}
