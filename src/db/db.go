package db

import (
  "database/sql"
  _ "github.com/go-sql-driver/mysql"
  "gopkg.in/gorp.v1"
  "fmt"
  "io/ioutil"
  "path/filepath"
  "strings"
)

var dbase *sql.DB
var Dbmap *gorp.DbMap

func Connect() {
  filename, _ := filepath.Abs("./db/dbconf.yml")
  yamlFile, err := ioutil.ReadFile(filename)

  if err != nil {
      panic(err)
  }

  f := strings.Split(string(yamlFile), "\n")

  //open database
  var adapter = strings.Trim(strings.Split(f[1], "driver:")[1], " ")
  var url2 = strings.Trim(strings.Split(f[2], "open:")[1]," ")

  dbase, err := sql.Open(adapter, url2)
  if err != nil {
    fmt.Println("Oh no")
    fmt.Println(err)
  }

  err = dbase.Ping()
  if err != nil {
    fmt.Println("Cannot ping db")
    panic(err.Error())
  }

  Dbmap = &gorp.DbMap{Db: dbase, Dialect: gorp.MySQLDialect{"InnoDB", "UTF8"}}
}

func Disconnect() {
  dbase.Close()
}
