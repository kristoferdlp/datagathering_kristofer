package lib

import (
  "os"
  "db"
  "lib/state"
  "lib/companysize"
  "lib/company"
  "lib/contact"
  "lib/email"
)

func Init() {
  db.Dbmap.AddTableWithName(company.Company{}, "company").SetKeys(true, "Id")
  db.Dbmap.AddTableWithName(companysize.Company_size{}, "company_sizes").SetKeys(true, "Id")
  db.Dbmap.AddTableWithName(contact.Contact{}, "contacts").SetKeys(true, "Id")
  db.Dbmap.AddTableWithName(email.Email{}, "emails").SetKeys(true, "Id")
  t5 := db.Dbmap.AddTableWithName(state.State{}, "states")
  t5.SetKeys(true, "Id")
  t5.ColMap("Name").SetUnique(true)
}

func Reset() {
  db.Dbmap.TruncateTables()
  os.RemoveAll("output")
  os.Mkdir("output", 0777)
}
