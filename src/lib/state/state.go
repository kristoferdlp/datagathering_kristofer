package state

import (
    "db"
    "fmt"
)

type State struct {
  Id int32
  Name string
}

func Create(name string) {
  var new_state = State{Name: name}
  add(new_state)
}

func add(s State) {
  err := db.Dbmap.Insert(&s)
  if (err != nil) {
    fmt.Println(err)
  }
}

func Empty() bool {
  count, _ := db.Dbmap.SelectInt("select count(*) from posts")
  return (count == 0)
}
