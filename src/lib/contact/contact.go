package contact

import (
  "time"
)

type Contact struct {
  Id int32
  First_name string
  Last_name string
  Data_com_url string
  Company_id int32
  State_id int32
  Company_size_id int32
  Created_at time.Time
  Updated_at time.Time
  Email_generated bool
  Priority string
}
