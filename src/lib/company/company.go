package company

import (
  "time"
)

type Company struct {
  Id int32
  Name string
  Site_url string
  Data_com_url string
  State_id int32
  Company_size_id int32
  Gathered int32
  Created_at time.Time
  Updated_at time.Time
  Priority string
}
