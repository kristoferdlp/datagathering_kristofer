package companysize

import (
    "db"
    "fmt"
)

type Company_size struct {
  Id int32
  Name string
}

func Create(name string) {
  var new_company_size = Company_size{Name: name}
  add(new_company_size)
}

func add(s Company_size) {
  err := db.Dbmap.Insert(&s)
  if (err != nil) {
    fmt.Println(err)
  }
}

func Empty() bool {
  count, _ := db.Dbmap.SelectInt("select count(*) from companysize")
  return (count == 0)
}
