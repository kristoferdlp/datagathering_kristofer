package loader

import (
  "lib/companysize"
  "io/ioutil"
  "path/filepath"
  "lib/state"
  "lib/companysize"
  "os"
)
//
// def initialize(source, company_size, priority)
//   @source = source
//   @company_size = company_size
//   @priority = priority
//   preload_database
// end

var Source string
var Company_size string
var Priority string

func Initialize(source string, company_size string, priority string) {
  Source = source
  Company_size = company_size
  Priority = priority
  preload_database()
}
// def file?(source)
//   File.file?(source)
// end

func isFile(source string) bool {
  f, _ := os.Open(source)
  fi, _ := f.Stat()
  mode := fi.Mode();
  return mode.IsRegular()
}
// def import
//   directories = fetch_directories
//
//   directories.each do |directory|
//     state = File.basename(directory)
//     Dir.glob("#{directory}**/*.csv") do |file_name|
//       Record.new(file_name, state, @company_size, @priority)
//     end
//   end
// end
func import() {
  dir = fetch_directories(Source)
  for i,v := dir {
    fName := filepath.Base(v)
    extName := filepath.Ext(v)
    state := fName[:len(fName)-len(extName)]
    dir2 = fetch_directories(v)
    for i2,v2 := dir2 {
      record.New(v2, state, Company_size, Priority)
    }
  }
}
// def fetch_directories
//   directories = []
//   Dir.glob("#{@source}**/*") do |folder|
//     directories << folder
//   end
//   directories
// end

func fetch_directories(source string) ][]string{
  dir,_ := filepath.Glob(source)
  return dir
}
// def import_from_file(source, state, company_size)
//   Record.new(source, state, company_size)
// end
func import_from_file(source string, state string, company_size string) {
  record.New(source, state, company_size)
}
// def harvest_site_urls
//   companies = Company.where(gathered: false)
//   gatherer = Gatherer.new(companies)
//   gatherer.run
// end
func harvest_site_urls() {
  companies = company.Where(false)
  gatherer_ = gatherer.new(companies)
  gatherer_.Run()
}
// def generate_email_leads
//   contacts = Contact.where(email_generated: false)
//   eg = EmailGenerator.new(contacts, @company_size)
//   eg.run
// end
func generate_email_leads() {
  contacts = contact.Where(false)
  eg = emailGenerator.New(contacts, Company_size)
  eg.Run()
}

func load_states(name string) state {
  filename, _ := filepath.Abs("assets/states.yml")
  file, err := ioutil.ReadFile(filename)
  for i,v := file {
    state.Create(strings.split(v, "- ")[1])
  }
}

func load_company_sizes(name string) Company_size {
  companysize.Create("small")
  companysize.Create("large")
}

func preload_database() {
  if state.Empty() && companysize.Empty() {
    load_states()
    load_company_sizes()
  }
}
