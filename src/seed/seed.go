package seed

import (
  "db"
  "fmt"
  "lib"
  "lib/state"
  "lib/companysize"
)

func Seed() {
  db.Connect()
  fmt.Println("Seeding...")
  lib.Init()

  // Populate State Records
  state.Create("Alabama")
  state.Create("Alaska")
  state.Create("Arizona")
  state.Create("Arkansas")
  state.Create("California")
  state.Create("Colorado")
  state.Create("Connecticut")
  state.Create("Delaware")
  state.Create("Florida")
  state.Create("Georgia")
  state.Create("Hawaii")
  state.Create("Idaho")
  state.Create("Illinois")
  state.Create("Indiana")
  state.Create("Iowa")
  state.Create("Kansas")
  state.Create("Kentucky")
  state.Create("Louisiana")
  state.Create("Maine")
  state.Create("Maryland")
  state.Create("Massachusetts")
  state.Create("Michigan")
  state.Create("Minnesota")
  state.Create("Mississippi")
  state.Create("Missouri")
  state.Create("Montana")
  state.Create("Nebraska")
  state.Create("Nevada")
  state.Create("Create Hampshire")
  state.Create("Create Jersey")
  state.Create("Create Mexico")
  state.Create("Create York")
  state.Create("North Carolina")
  state.Create("North Dakota")
  state.Create("Ohio")
  state.Create("Oklahoma")
  state.Create("Oregon")
  state.Create("Pennsylvania")
  state.Create("Rhode Island")
  state.Create("South Carolina")
  state.Create("South Dakota")
  state.Create("Tennessee")
  state.Create("Texas")
  state.Create("Utah")
  state.Create("Vermont")
  state.Create("Virginia")
  state.Create("Washington")
  state.Create("West Virginia")
  state.Create("Wisconsin")
  state.Create("Wyoming")

  // Populate Company Size Records
  companysize.Create("small")
  companysize.Create("large")

  // state_all()
  //db.Disconnect()

}
