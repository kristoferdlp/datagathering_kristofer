Installation Instructions

1. Install Go (v1.6)
2. Install mysql
3. Create database leads_manager_development
4. Install Goose (run: go get bitbucket.org/liamstask/goose/cmd/goose)
5. Install gorp (run: go get gopkg.in/gorp.v1)
6. Install go-sql-driver (run: go get github.com/go-sql-driver/mysql)
7. Apply migrations (run: bin/goose up)
